<?php

declare(strict_types=1);

namespace Booking\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="booking")
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=32, nullable=false)
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(name="reason", type="string", length=32, nullable=false)
     * @var string
     */
    private $reason;

    /**
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     * @var datetime
     */
    private $start_time;

    /**
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     * @var datetime
     */
    private $end_time;

    /**
     * @return array
     */
    public function getBooking(): array
    {
        $booking = [
            'id' => $this->getId(),
            'username' => $this->getUsername(),
            'reason' => $this->getReason(),
            'start_time' => $this->getStartTime()->format('Y-m-d H:i:s'),
            'end_time' => $this->getEndTime()->format('Y-m-d H:i:s'),
        ];

        return $booking;
    }

    /**
     * @param array $requestBody
     * @throws \Exception
     */
    public function setBooking(array $requestBody): void
    {
        // required data fields
        $this->setUsername($requestBody['username']);
        $this->setReason($requestBody['reason']);
        $this->setEndTime(new \DateTime("now"));
    }

    /**
    * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime(): \DateTime
    {
        return $this->start_time;
    }

    /**
     * @param \DateTime $start_time
     * @throws \Exception
     */
    public function setStartTime(\DateTime $start_time = null): void
    {
        if (!$start_time && empty($this->getId())) {
            $this->start_time = new \DateTime("now");
        } else {
            $this->start_time = $start_time;
        }
    }

    /**
     * @return \DateTime
     */
    public function getEndTime(): \DateTime
    {
        return $this->end_time;
    }

    /**
     * @param \DateTime $end_time
     * @throws \Exception
     */
    public function setEndTime(\DateTime $end_time = null): void
    {
        if (!$end_time) {
            $this->end_time = new \DateTime("now");
        } else {
            $this->end_time = $end_time;
        }
    }

}