<?php

declare(strict_types=1);

namespace Booking\Handler;

use Doctrine\ORM\ORMException;
use Zend\Expressive\Helper\ServerUrlHelper;
use Doctrine\ORM\EntityManager;
use Booking\Entity\Booking;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 *
 * Class BranchesCreateHandler
 *
 * Example request body to create can be found in /data/booking_create.json
 */
class BookingCreateHandler implements RequestHandlerInterface
{

    protected $entityManager;

    protected $entity;

    protected $urlHelper;

    /**
     * BookingCreateHandler constructor.
     * @param EntityManager $entityManager
     * @param Booking $entity
     * @param ServerUrlHelper $urlHelper
     */

    public function __construct(
        EntityManager $entityManager,
        Booking $entity,
        ServerUrlHelper $urlHelper
    ) {
        $this->entityManager = $entityManager;
        $this->entity = $entity;
        $this->urlHelper = $urlHelper;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $result = [];

        $requestBody = $request->getParsedBody()['Doctor']['Booking'];

        if (empty($requestBody)) {
            $result['_error']['error'] = 'missing_request';
            $result['_error']['error_description'] = 'No request body sent.';

            return new JsonResponse($result, 400);
        }

        try {
            $this->entity->setBooking($requestBody);
            $this->entity->setStartTime(new \DateTime("now"));

            $this->entityManager->persist($this->entity);
            $this->entityManager->flush();
        } catch(ORMException $e) {
            $result['_error']['error'] = 'not_created';
            $result['_error']['error_description'] = $e->getMessage();

            return new JsonResponse($result, 400);
        }

        // add hypermedia links
        $result['Result']['_links']['self'] = $this->urlHelper->generate('/booking/'.$this->entity->getId());
        $result['Result']['_links']['read'] = $this->urlHelper->generate('/booking/');
        $result['Result']['_links']['update'] = $this->urlHelper->generate('/booking/'.$this->entity->getId());
        $result['Result']['_links']['delete'] = $this->urlHelper->generate('/booking/'.$this->entity->getId());

        $result['Result']['_embedded']['Booking'] = $this->entity->getBooking();

        if (empty($result['Result']['_embedded']['Booking'])) {
            $result['_error']['error'] = 'not_created';
            $result['_error']['error_description'] = 'Not Created.';

            return new JsonResponse($result, 400);
        }

        return new JsonResponse($result, 201);
    }
}
