<?php

declare(strict_types=1);

namespace Booking\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

class BookingDeleteHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return BookingDeleteHandler
     */
    public function __invoke(ContainerInterface $container) : BookingDeleteHandler
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default'); //EntityManager::class

        $entityRepository = $entityManager->getRepository('Booking\Entity\Booking');

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new BookingDeleteHandler($entityManager, $entityRepository, $urlHelper);
    }
}
