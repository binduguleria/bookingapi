<?php

declare(strict_types=1);

namespace Booking\Handler;

use Doctrine\ORM\EntityManager;
use Booking\Entity\Booking;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

/**
 * Class BranchesUpdateHandlerFactory
 * @package Branches\Handler
 */
class BookingUpdateHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return BookingUpdateHandler
     */
    public function __invoke(ContainerInterface $container) : BookingUpdateHandler
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        $entityRepository = $entityManager->getRepository('Booking\Entity\Booking');

        $entity = new Booking();

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new BookingUpdateHandler($entityManager, $entityRepository, $entity, $urlHelper);
    }
}
