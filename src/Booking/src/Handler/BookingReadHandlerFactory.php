<?php

declare(strict_types=1);

namespace Booking\Handler;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;


class BookingReadHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return BookingReadHandler
     */
    public function __invoke(ContainerInterface $container) : BookingReadHandler
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        $query = $entityManager->getRepository('Booking\Entity\Booking')
            ->createQueryBuilder('c')
            ->getQuery();

        $paginator  = new Paginator($query);

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new BookingReadHandler($paginator, 5, $urlHelper);
    }
}