<?php

declare(strict_types=1);

namespace Booking\Handler;

use Doctrine\ORM\EntityManager;
use Booking\Entity\Booking;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

/**
 * Class BookingCreateHandlerFactory
 * @package Booking\Handler
 */
class BookingCreateHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return BookingCreateHandler
     */
    public function __invoke(ContainerInterface $container) : BookingCreateHandler
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        $entity = new Booking();

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new BookingCreateHandler($entityManager, $entity, $urlHelper);
    }
}
