<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;
use Booking\Handler\BookingDeleteHandler;
use Booking\Handler\BookingUpdateHandler;
use Booking\Handler\BookingReadHandler;
use Booking\Handler\BookingCreateHandler;


/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void {
    $app->get('/', App\Handler\HomePageHandler::class, 'home');
    $app->get('/api/ping', App\Handler\PingHandler::class, 'api.ping');

    $app->get('/booking/[page/{page:\d+}]', BookingReadHandler::class, 'booking.read');
    $app->post('/booking[/]', BookingCreateHandler::class, 'booking.create');
//    $app->get('/branches/{id:\d+}[/]', Handler\BranchesViewHandler::class, 'branches.view');
    $app->put('/booking/{id:\d+}[/]', BookingUpdateHandler::class, 'booking.update');
    $app->delete('/booking/{id:\d+}[/]', BookingDeleteHandler::class, 'booking.delete');
};
