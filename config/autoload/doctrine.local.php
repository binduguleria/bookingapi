<?php
/**
 * Created by PhpStorm.
 * User: bindu
 * Date: 2019-03-09
 * Time: 1:08 PM
 */
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params' => [
                    'url' => 'mysql://root:@localhost/doctor',
//                    'driver' => 'pdo_mysql',
//                    'host' => 'localhost', // uses the name of the container from docker-compose
//                    'dbname' => 'doctor',
//                    'user' => 'root',
//                    'password' => '',
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
                    'Booking\Entity' => 'my_entity',
                ],
            ],
            'my_entity' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => __DIR__ . '/../../src/Booking/Entity',
            ],
        ],
    ],
];